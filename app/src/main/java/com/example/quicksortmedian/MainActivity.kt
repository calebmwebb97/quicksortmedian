package com.example.quicksortmedian

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    val array = intArrayOf(5, 12, 2, 54, 53, 17, 3, 8, 1, 27, 4, 15, 9)

    var countLeft = 0
    var countRight = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        println(findMedian(array))
    }

    fun findMedian(array: IntArray) : Int{
        println(array.contentToString())
        val med = partition(array, 0, array.size-1)
        println(array.contentToString())
        return med
    }

    fun partition(array: IntArray, l: Int, r: Int): Int {
        var left = l
        var right = r
        val pivot = array[(left + right)/2] // 4) Pivot Point
        var median = 0
        //println(array.indexOf(pivot))
        while (left <= right) {
            while (array[left] < pivot) left++ // 5) Find the elements on left that should be on right

            while (array[right] > pivot) right-- // 6) Find the elements on right that should be on left

            // 7) Swap elements, and move left and right indices
            if (left <= right) {
                swapArray(array, left,right)
                left++
                right--
            }
        }
        //println(array.indexOf(pivot))
        countLeft = array.indexOf(pivot)
        countRight = array.size-array.indexOf(pivot)-1
        if (countLeft > countRight)
        {
            median = partition(array, l, array.indexOf(pivot))
        }
        else if (countLeft < countRight)
        {
            median = partition(array, array.indexOf(pivot), r)
        }
        else
        {
            median = pivot
        }
        //println(median)
        return median
    }

    fun swapArray(a: IntArray, b: Int, c: Int) {
        val temp = a[b]
        a[b] = a[c]
        a[c] = temp
    }
}
